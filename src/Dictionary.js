import React, { Component } from 'react';
import Word from './Word';

class Dictionary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dictionary: this.getDictionary()
    };
  }

  getDictionary() {
    /* As per the note in notes.txt, CORS is not enabled for the supplied URL, hence me hard-coding it
    let request = new XMLHttpRequest();
    request.open('GET', 'https://rnbp-development.firebaseapp.com/dictionary.json', true);
    request.send(null);
    return request.responseText; */

    return {
      "atelier": "a workshop or studio, especially of an artist, artisan, or designer.",
      "tsuries": "Slang. trouble; woe.",
      "misinformation": "false information that is spread, regardless of whether there is intent to mislead: In the chaotic hours after the earthquake, a lot of misinformation was reported in the news.",
      "serry": "Archaic. to crowd closely together.",
      "waggish": "roguish in merriment and good humor; jocular; like a wag: Fielding and Sterne are waggish writers.",
      "doorbuster": "Informal. a retail item that is heavily discounted for a very limited time in order to draw customers to the store. b. the price of such an item.",
      "thanksgiver": "a person who gives thanks.",
      "diaeresis": "A mark (¨) placed over a vowel to indicate that it is sounded separately, as in naïve, Brontë."
    };
  }

  renderDictionaryEntry() {

  }

  render() {
    const entries = this.state.dictionary;
    const wordDefinitionPairs = Object.keys(entries).map((dictionaryWord, index) => {
      return (
        <li key={index}>
          <Word
            word={dictionaryWord}
            definition ={entries[index]}
          >
          </Word>
        </li>
      )
    });
    return (
      <ul className="Dictionary">
        {wordDefinitionPairs}
      </ul>
    );
  }
}

export default Dictionary;