import React, { Component } from 'react';

class Word extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDefinition: false
    };
  }

  //The parent shouldn't need to worry if the definition is showing or not, hence keep it at this level
  toggleShowDefinition() {
      const toggledShowDefinition = !this.state.showDefinition;
      this.setState({
              showDefinition: toggledShowDefinition
          }
      )
  }

  render() {
    //NEVER MUTATE
    const showDefinition = this.state.showDefinition;
    
    return (
        <div>
            <span onClick={() => this.toggleShowDefinition()}>{this.props.word}</span>
            {showDefinition &&
                <p>{this.props.definition}</p>
            }
        </div>
    );
  }
}

export default Word;